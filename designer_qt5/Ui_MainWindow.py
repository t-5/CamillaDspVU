# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Ui_MainWindow.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(160, 500)
        MainWindow.setMinimumSize(QtCore.QSize(160, 300))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/MainWindow/icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.toolButtonAbout = QtWidgets.QToolButton(self.centralWidget)
        self.toolButtonAbout.setGeometry(QtCore.QRect(4, 4, 28, 31))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/MainWindow/help-about.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButtonAbout.setIcon(icon1)
        self.toolButtonAbout.setObjectName("toolButtonAbout")
        self.toolButtonExit = QtWidgets.QToolButton(self.centralWidget)
        self.toolButtonExit.setGeometry(QtCore.QRect(38, 4, 28, 31))
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/MainWindow/exit.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButtonExit.setIcon(icon2)
        self.toolButtonExit.setObjectName("toolButtonExit")
        self.errorLabel = QtWidgets.QLabel(self.centralWidget)
        self.errorLabel.setGeometry(QtCore.QRect(10, 40, 131, 101))
        self.errorLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.errorLabel.setWordWrap(True)
        self.errorLabel.setObjectName("errorLabel")
        self.toolButtonReset = QtWidgets.QToolButton(self.centralWidget)
        self.toolButtonReset.setGeometry(QtCore.QRect(80, 4, 28, 31))
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/MainWindow/reload.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButtonReset.setIcon(icon3)
        self.toolButtonReset.setObjectName("toolButtonReset")
        MainWindow.setCentralWidget(self.centralWidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "CamillaDSP VU-meter by T5!"))
        self.toolButtonAbout.setToolTip(_translate("MainWindow", "About..."))
        self.toolButtonAbout.setText(_translate("MainWindow", "..."))
        self.toolButtonAbout.setShortcut(_translate("MainWindow", "F1"))
        self.toolButtonExit.setToolTip(_translate("MainWindow", "Exit!"))
        self.toolButtonExit.setText(_translate("MainWindow", "..."))
        self.errorLabel.setText(_translate("MainWindow", "Starting..."))
        self.toolButtonReset.setToolTip(_translate("MainWindow", "Reset peak values"))
        self.toolButtonReset.setText(_translate("MainWindow", "..."))
import CamillaDspVU_rc
