#!/bin/bash

for ui in *.ui; do
    echo reading $ui...
    py=`echo $ui | sed s#.ui#.py#`
    echo writing $py...
    pyuic5 $ui -o $py
done

echo
cd res
find . -iname "*.png" | while read f; do convert "${f}" -strip "${f}"; done
pyrcc5 -o ../CamillaDspVU_rc/__init__.py CamillaDspVU.qrc
cd ..
