import argparse
import json
import math
import os
import sys
from pkg_resources import parse_version
from tempfile import mkstemp

from helpers.exceptions import InvalidResponse, VersionTooSmall

try:
    from websocket import create_connection
    from websocket import WebSocketException
except ImportError:
    create_connection = WebSocketException = None

from PyQt5.QtCore import QSettings, QTimer, pyqtSignal
from PyQt5.QtGui import QCloseEvent, QFont
from PyQt5.QtWidgets import QLabel, QMainWindow, QMessageBox, QSizePolicy

from qt5_levelmeter import QLevelMeter, QLevelMeterRms
from WindowClasses.AboutDialog import AboutDialog
from designer_qt5.Ui_MainWindow import Ui_MainWindow
from helpers.UpdateThread import UpdateThread
from helpers.constants import MIN_CDSP_VERSION, WEBSOCKET_PORT


class MainWindow(QMainWindow, Ui_MainWindow):

    signal_stop_update_thread = pyqtSignal(str)


    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self._running = False
        self._channelCount = 0
        self._meters = []
        self._lastTimestamp = 0
        self._fastRolloff = False
        self._showRms = False
        self._updateThread = None
        self._settings = QSettings("CamillaDspVU", "CamillaDspVU")
        self._setupStatusBar()
        self._bindActions()
        self._readIniSettings()
        ## do module import checks
        self._websocketClientImportCheck()
        ## setup update status bar timer
        self._timerUpdateStatusbar = QTimer(self)
        self._timerUpdateStatusbar.setInterval(50)
        # noinspection PyUnresolvedReferences
        self._timerUpdateStatusbar.timeout.connect(self._updateStatusBarHandler)
        self._timerUpdateStatusbar.start()
        # noinspection PyUnresolvedReferences
        self.signal_stop_update_thread.connect(self.stopUpdateThread)
        ## parse command line arguments
        parser = argparse.ArgumentParser(prog='camilladsp-vu')
        parser.add_argument("--port", nargs="?", help="camilladsp websocket port (defalt: 7964)")
        parser.add_argument("--fast", help="Fast rolloff (25/s instead of 20dB/1.7s)", action="store_true")
        parser.add_argument("--rms", help="Show RMS level bars", action="store_true")
        parser.add_argument("--max-channels", help="Maximum number of channels to show")
        try:
            args = parser.parse_args()
            if args.port:
                self._websocket_port = args.port
            else:
                self._websocket_port = WEBSOCKET_PORT
            if args.fast:
                self._fastRolloff = True
            if args.rms:
                self._showRms = True
            if args.max_channels:
                try:
                    self._maxChannels = int(args.max_channels)
                except ValueError:
                    print("ERROR: max-channels Argument must be a number")
                    sys.exit(1)
            else:
                self._maxChannels = 0
        except argparse.ArgumentError:
            parser.print_help()
            exit(1)


    def closeEvent(self, evt: QCloseEvent):
        """ do stuff necessary before closing tthe main window """
        self._writeIniSettings()
        if self._updateThread:
            self._lastTimestamp = self._updateThread.stop()
            self._updateThread.wait()


    def onExit(self):
        """ exit! """
        self.close()


    def onResetPeakValues(self):
        if not self._updateThread:
            return
        self._updateThread.resetPeaks()
        for meter in self._meters:
            meter.resetPeakLevelMax()


    @staticmethod
    def onShowAboutDialog():
        """ show about dialog """
        dlg = AboutDialog()
        dlg.exec_()


    def resizeEvent(self, evt):
        # noinspection PyUnresolvedReferences
        h = self.centralWidget.height() - 46
        for meter in self._meters:
            geo = meter.geometry()
            x = geo.x()
            y = geo.y()
            w = geo.width()
            meter.setGeometry(x, y, w, h)
        # noinspection PyUnresolvedReferences
        self.errorLabel.setGeometry(4, 44, self.centralWidget.width() - 8, self.centralWidget.height() - 70)
        self._statusbarText.setFixedWidth(self.centralWidget.width() - 4)


    def settings(self) -> QSettings:
        return self._settings


    def stopUpdateThread(self, errorMessage):
        if self._updateThread:
            self._lastTimestamp = self._updateThread.stop()
        self._teardownMeters()
        self._running = False
        self.errorLabel.setText(errorMessage + "\n\nWaiting for camilladsp (retrying every 5s)...")
        self._statusbarText.setText(" Not running")


    def updateMeters(self, peakValues: list, rmsValues: list):
        if len(peakValues) < self._channelCount or len(rmsValues) < self._channelCount:
            return
        for i in range(min(self._channelCount, len(peakValues))):
            self._meters[i].levelChanged(peakValues[i], rmsValues[i])


    def _bindActions(self):
        ## menu actions
        self.toolButtonAbout.clicked.connect(self.onShowAboutDialog)
        self.toolButtonExit.clicked.connect(self.onExit)
        self.toolButtonReset.clicked.connect(self.onResetPeakValues)


    def _readIniSettings(self):
        """ read ini settings """
        s = self._settings
        # main window geometry
        geo = s.value("main/mainWindowGeo")
        if geo is not None:
            self.restoreGeometry(geo)
        state = s.value("main/mainWindowState")
        if state is not None:
            self.restoreState(state)


    def _setupMeters(self):
        self._teardownMeters()
        self.errorLabel.hide()
        cw = self.centralWidget
        # noinspection PyUnresolvedReferences
        cwh = cw.height()
        for i in range(self._channelCount):
            # noinspection PyTypeChecker
            if self._showRms:
                meter = QLevelMeterRms(cw, self._fastRolloff)
            else:
                meter = QLevelMeter(cw, self._fastRolloff)
            meter.setGeometry(4 + i * 66, 46, 60, cwh - 46)
            self._meters.append(meter)
            meter.show()
        channels = max(2, self._channelCount)
        self.setFixedWidth(16 + channels * 60 + (channels - 1) * 6)
        self._updateThread = UpdateThread(self, websocket_port=self._websocket_port)
        self._updateThread.start()


    def _setupStatusBar(self):
        self._statusbarText = QLabel()
        self._statusbarText.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        # sizes and font sizes
        self._statusbarText.setStyleSheet("background-color: #171717;")
        font = QFont("sans-serif", 10)
        self._statusbarText.setFont(font)
        # add widgets to status bar
        sb = self.statusBar()
        sb.addWidget(self._statusbarText)
        self._statusbarText.setText(" Not running")


    def _teardownMeters(self):
        for meter in self._meters:
            meter.hide()
            meter.deleteLater()
        self._meters = []
        self.errorLabel.show()


    def _updateStatusBarHandler(self):
        self._timerUpdateStatusbar.stop()
        ws = None
        try:
            if not self._running:
                # check version
                ws = create_connection("ws://localhost:{}/".format(self._websocket_port))
                ws.send("\"GetVersion\"")
                result = ws.recv()
                data = json.loads(result)
                if "GetVersion" in data and data["GetVersion"]["result"] == "Ok":
                    version = data["GetVersion"]["value"]
                    if parse_version(version) < parse_version(MIN_CDSP_VERSION):
                        raise VersionTooSmall(MIN_CDSP_VERSION, version)
                else:
                    raise InvalidResponse("Could not find 'GetVersion' key in websocket response.")
                ws.close()
                # get channel count
                ws = create_connection("ws://localhost:{}/".format(self._websocket_port))
                ws.send("\"GetSignalPeaksSinceStart\"")
                result = ws.recv()
                data = json.loads(result)
                if "GetSignalPeaksSinceStart" in data and data["GetSignalPeaksSinceStart"]["result"] == "Ok":
                    self._channelCount = len(data["GetSignalPeaksSinceStart"]["value"]["playback"])
                    if self._maxChannels:
                        self._channelCount = min(self._channelCount, self._maxChannels)
                    self._statusbarText.setText(" Running, {} Channels".format(self._channelCount))
                    if not self._running:
                        self._setupMeters()
                        self._running = True
                    peakDBs = list(map(lambda x: x > 0 and 20 * math.log10(x) or -1000.0, data["GetSignalPeaksSinceStart"]["value"]["playback"]))
                    self.updateMeters(peakDBs, [-1000.0] * self._channelCount)
                else:
                    raise InvalidResponse("Could not find 'GetSignalPeaksSinceStart' key in websocket response.")
        except (json.JSONDecodeError, WebSocketException, ConnectionRefusedError, VersionTooSmall, InvalidResponse) as e:
            self.stopUpdateThread(str(e))
        finally:
            if ws:
                ws.close()
        self._timerUpdateStatusbar.setInterval(5000)
        self._timerUpdateStatusbar.start()


    def _websocketClientImportCheck(self):
        try:
            import websocket
        except ImportError:
            dlg = QMessageBox()
            dlg.setStyleSheet(self.styleSheet())
            msg = "WARNING:\n"
            msg += "  Could not find python module 'websocket-client'.\n"
            msg += "  Should we try to install it for you?\n"
            msg += "  (This may take a few seconds or minutes...)"
            result = dlg.warning(self, "Dependencies warning", msg, QMessageBox.Yes | QMessageBox.No)
            if result == QMessageBox.Yes:
                break_system_packages = sys.version_info[1] >= 11 and "--break-system-packages" or ""
                ret = os.system("pip3 install %s websocket-client" % break_system_packages)
                if ret != 0:
                    msg = "ERROR: module 'websocket-client' not found.\n"
                    msg += "  Manual installation: 'pip3 install websocket-client'"
                    dlg.critical(self, "ERROR loading dependencies!", msg)
                    print(msg, file=sys.stderr)
                    sys.exit(1)
                else:
                    msg = "SUCCESS:\n"
                    msg += "  The websocket-client module was successfully installed.\n"
                    msg += "  The program will be restarted now..."
                    dlg.information(self, "Need to restart...", msg, QMessageBox.Ok)
                    fd, fn = mkstemp()
                    os.close(fd)
                    os.chmod(fn, 0o700)
                    f = open(fn, "w")
                    f.write("#!/bin/sh\nsleep 2\n/usr/bin/camilladsp-vu\nrm -f %s\n" % fn)
                    f.close()
                    os.system("%s &" % fn)
                    sys.exit(0)
            else:
                sys.exit(1)


    def _writeIniSettings(self):
        """ write ini settings """
        s = self._settings
        s.setValue("main/mainWindowGeo", self.saveGeometry())
        s.setValue("main/mainWindowState", self.saveState())
