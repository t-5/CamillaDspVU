import os
import sys
from datetime import datetime

from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QDesktopServices
from PyQt5.QtWidgets import QDialog
from designer_qt5.Ui_AboutDialog import Ui_AboutDialog
from qt5_t5darkstyle import darkstyle_css

from helpers.constants import CURRENT_VERSION


class AboutDialog(QDialog, Ui_AboutDialog):
    def __init__(self):
        super(AboutDialog, self).__init__()
        self.setupUi(self)
        self.setStyleSheet(darkstyle_css())
        self.label.setText(self.label.text().replace("$version", CURRENT_VERSION))
        year = datetime.now().strftime("%Y")
        if year != "2021":
            year = "2021-" + year
        self.label_2.setText(self.label_2.text().replace("$year", year))
        self.label_4.mousePressEvent = self.emailClicked
        self.label_5.mousePressEvent = self.licenceClicked
        self.setModal(True)


    @staticmethod
    def emailClicked(_=None):
        url = QUrl('mailto:t-5@t-5.eu')
        QDesktopServices().openUrl(url)


    @staticmethod
    def licenceClicked(_=None):
        url = QUrl(os.path.join(os.path.dirname(sys.argv[0]), "licence.txt"))
        QDesktopServices().openUrl(url)
