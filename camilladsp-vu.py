#!/usr/bin/python3

"""
CamillaDspVU
============
A VU-meter for camilladsp's outputs

Main python (startup) file

GIT: https://gitlab.com/t-5/CamillaDspVU.git
"""

from PyQt5 import QtWidgets
from qt5_t5darkstyle import darkstyle_css
import sys
from WindowClasses.MainWindow import MainWindow


def main():
    app = QtWidgets.QApplication(sys.argv)
    form = MainWindow()
    form.setStyleSheet(darkstyle_css())
    form.show()
    app.exec_()


if __name__ == '__main__':
    main()
