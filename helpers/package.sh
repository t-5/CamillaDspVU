#!/bin/bash

cd ..
VERSION=$(cat .previous_version)
echo -n "Version (previous version was $VERSION) : "
read -r VERSION

sudo rm -f builds/*.deb
sudo rm -rdf debian

sudo mkdir -p debian/DEBIAN
sudo mkdir -p debian/usr/bin
sudo mkdir -p debian/usr/share/applications
sudo mkdir -p debian/usr/share/menu
sudo mkdir -p debian/usr/share/pixmaps
sudo mkdir -p debian/usr/share/mime/packages
for d in 16 24 32 48 64 96 128 256 512; do
    sudo mkdir -p debian/usr/share/icons/hicolor/${d}x${d}/mimetypes
done
sudo mkdir -p debian/usr/share/icons/hicolor/scalable/mimetypes
sudo mkdir -p debian/usr/lib/camilladsp-vu/CustomWidgets
sudo mkdir -p debian/usr/lib/camilladsp-vu/designer_qt5
sudo mkdir -p debian/usr/lib/camilladsp-vu/helpers
sudo mkdir -p debian/usr/lib/camilladsp-vu/CamillaDspVU_rc
sudo mkdir -p debian/usr/lib/camilladsp-vu/WindowClasses
sudo mkdir -p debian/usr/share/doc/camilladsp-vu


< control.in sed "s#Version: _VERSION_#Version: ${VERSION}#" > control
sudo mv -f control debian/DEBIAN/control
sudo cp debian.in/postinst debian/DEBIAN
sudo cp debian.in/postrm debian/DEBIAN
sudo cp debian.in/changelog debian/usr/share/doc/camilladsp-vu
sudo gzip --best --no-name debian/usr/share/doc/camilladsp-vu/changelog
sudo cp debian.in/*.desktop debian/usr/share/applications
sudo cp debian.in/*.menu debian/usr/share/menu
for d in 16 24 32 48 64 96 128 256 512; do
    sudo cp -f debian.in/icon_${d}x${d}.png debian/usr/share/icons/hicolor/${d}x${d}/mimetypes/application-camilladsp-vu.png
done
sudo cp debian.in/*.svg debian/usr/share/icons/hicolor/scalable/mimetypes
sudo cp debian.in/*.svg debian/usr/share/pixmaps

sudo cp -f camilladsp-vu debian/usr/bin
sudo cp -f CustomWidgets/*.py debian/usr/lib/camilladsp-vu/CustomWidgets
sudo cp -f designer_qt5/*.py debian/usr/lib/camilladsp-vu/designer_qt5
sudo cp -f helpers/*.py debian/usr/lib/camilladsp-vu/helpers
sudo cp -f CamillaDspVU_rc/*.py debian/usr/lib/camilladsp-vu/CamillaDspVU_rc
sudo cp -f WindowClasses/*.py debian/usr/lib/camilladsp-vu/WindowClasses
sudo cp -f licence.txt debian/usr/lib/camilladsp-vu
sudo cp -f camilladsp-vu.py debian/usr/lib/camilladsp-vu/camilladsp-vu.py

sudo chown -R root:root debian/
sudo chmod -R o+rX debian/
sudo dpkg-deb -Zxz --build debian && \
    mv debian.deb builds/camilladsp-vu_"${VERSION}"_all.deb && \
    echo -n "$VERSION" > .previous_version

sudo chown -R jh:jh debian/
sudo chown -R jh:jh builds/
