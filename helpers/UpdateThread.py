import _thread
import json
from time import sleep

from helpers.constants import WEBSOCKET_PORT

try:
    import websocket
    from websocket import WebSocketException
except ImportError:
    websocket = WebSocketException = None

from PyQt5 import QtCore


class UpdateThread(QtCore.QThread):

    def __init__(self, parent=None, websocket_port=WEBSOCKET_PORT, lastTimestamp=0):
        super().__init__(parent=parent)
        self._lastTimestamp = lastTimestamp
        self._running = True
        self._ws = None
        self._processed = True
        self._needPeakReset = False
        self._websocket_port = websocket_port


    def run(self):
        self._processed = True

        def on_message(_, message):
            try:
                data = json.loads(message)
                if "GetSignalLevelsSinceLast" in data and \
                        data["GetSignalLevelsSinceLast"]["result"] == "Ok" and \
                        "playback_peak" in data["GetSignalLevelsSinceLast"]["value"]:
                            self.parent().updateMeters(
                                    data["GetSignalLevelsSinceLast"]["value"]["playback_peak"],
                                    data["GetSignalLevelsSinceLast"]["value"]["playback_rms"])
            except json.JSONDecodeError as e:
                self._running = False
                self.parent().signal_stop_update_thread.emit(str(e))
            finally:
                self._processed = True

        def on_error(_, error):
            self._running = False
            self.parent().signal_stop_update_thread.emit(str(error))

        def on_open(ws):
            def run(*_):
                while self._running:
                    try:
                        if self._processed:
                            self._processed = False
                            ws.send("\"GetSignalLevelsSinceLast\"")
                        if self._needPeakReset:
                            self._needPeakReset = False
                            ws.send("\"ResetSignalPeaksSinceStart\"")
                        sleep(0.05)
                    except (WebSocketException, ConnectionRefusedError) as e:
                        self._running = False
                        self.parent().signal_stop_update_thread.emit(str(e))
                ws.close()
            _thread.start_new_thread(run, ())

        ws = websocket.WebSocketApp("ws://localhost:{}/".format(self._websocket_port),
                                    on_open=on_open,
                                    on_message=on_message,
                                    on_error=on_error)
        ws.run_forever(skip_utf8_validation=False)


    def resetPeaks(self):
        self._needPeakReset = True


    def stop(self):
        self._running = False
        return self._lastTimestamp
