class VersionTooSmall(Exception):
    def __init__(self, required, version):
        self._required = required
        self._version = version

    def __str__(self):
        return "camilladsp version {} to old, need at least {}".format(self._version, self._required)


class InvalidResponse(Exception):
    pass
